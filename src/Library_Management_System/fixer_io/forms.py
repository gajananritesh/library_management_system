from django import forms

class CalculationForm(forms.Form):
    date = forms.DateField()
    opt1 = forms.CharField()
    opt2 = forms.CharField()
    value = forms.FloatField()
    