from django.db import models
from jsonfield import JSONField
import collections, datetime


class FixerExchangeRates(models.Model):
    aud = models.FloatField()
    inr = models.FloatField()
    cad = models.FloatField()
    cny = models.FloatField()
    usd = models.FloatField()
    date_time = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.date_time)

    class Meta:
        ordering = ('date_time',)

class FinalFixerModel(models.Model):
    
    date = models.DateField(auto_now_add=True, unique=True, null=True)
    date_time = models.DateTimeField(auto_now_add=True, null=True)
    data_json = JSONField(null=True)

    def __str__(self):
            return str(self.date)
