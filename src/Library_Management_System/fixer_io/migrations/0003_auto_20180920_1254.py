# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-20 07:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fixer_io', '0002_finalfixermodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='finalfixermodel',
            name='date',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
