from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^fixer/$', views.fixer_view, name='fixer'),
    url(r'^fixerdata/$', views.fixerdata_view, name='fixerdata'),
    url(r'^calculate/$', views.calculate_view, name='calculate'),
]