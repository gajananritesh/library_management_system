from django.shortcuts import render, HttpResponse
from .models import FixerExchangeRates, FinalFixerModel
import requests
from .forms import CalculationForm


def fixer_view(request):
    if request.method == 'POST':
        payload = {'access_key': '40e7aab9bcfd97ea90d4efb24fa770b1'}
        r = requests.get('http://data.fixer.io/api/latest', params=payload)
        data = r.json()     # data ia a dictionary
        data2 =  data['rates']
        FinalFixerModel.objects.create(data_json=data2)
        
        args = {'success_message':'Record Inserted Successfully',}
        return render(request, 'fixer.html', args)
    else:
        return render(request, 'fixer.html')

def fixerdata_view(request):

    if request.method == 'POST':
        data = request.POST['d']
        if data:
            data2 = FinalFixerModel.objects.get(date=data)

            dictdata = data2.data_json

            args = {'dictdata':dictdata}
            return render(request, 'fixer.html', args)
        else:
            args = {'error_message':'Please Select a Valid Date.'}
            return render(request, 'fixer.html', args)
    else:
        return render(request, 'fixer.html', args)

def calculate_view(request):
    if request.method == 'POST':
        form = CalculationForm(request.POST)
        
        if form.is_valid():
            opt1 = form.cleaned_data['opt1']     #USD
            opt2 = form.cleaned_data['opt2']     #INR
            value = form.cleaned_data['value']    #1
            date = form.cleaned_data['date']

            obj = FinalFixerModel.objects.get(date=date)

            dictdata = obj.data_json

            res1 = dictdata[opt1]
            res2 = dictdata[opt2]

            cal = res2/res1
            result = float(value)*cal

            args = {'res1':res1, 'res2':res2, 'dictdata':dictdata, 'result':result, 'form':form}
            return render(request, 'fixer.html', args)
            
        else:
            return render(request, 'fixer.html', {'error_message': 'Error while calculating...'})
    else:
        return render(request, 'fixer.html', {'error_message': 'Error while calculating...'})


