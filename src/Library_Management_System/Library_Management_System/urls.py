
from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from library import views

urlpatterns = [
    url(r'^library/', include('library.urls')),
    url(r'^fixer_io/', include('fixer_io.urls')),

    url(r'^admin/', admin.site.urls),
    
    url(r'^member/', views.memberList.as_view()),
    url(r'^books/', views.booksList.as_view()),

    url(r'^searchlist/$', views.searchList.as_view(), name='searchlist'),
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
