from django.contrib.auth.models import User
from django import forms
from models import Books, Member
from django.core.exceptions import ValidationError
import re

class LibrarianLoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (  'first_name',
                    'last_name',
                    'email',
                    'username',
                    'password',
                )
    def clean_first_name(self):
        fname = self.cleaned_data['first_name']
        if len(fname) < 3:
            raise ValidationError('first name must be more than two characters.')
        return fname
    
    def clean_email(self):
        email = self.cleaned_data['email']

        matchObj = re.match(r'^\D+@+\D+.com$', email)

        if not matchObj:
            raise ValidationError("Please Enter a Valid Email Address.")
        return email


class BookRegisterForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = (  'book_name',
                    'author_name',
                    'year',
                    'price',
                    'date',
                )

class MemberRegForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = (  'first_name',
                    'last_name',
                    'mobile_no',
                    'address',
                    'photo',
                )


