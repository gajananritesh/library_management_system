$(document).ready( function(){
    $( "#search" ).autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: "/searchlist",
            dataType: "json",
            data: {
                search: request.term
            },
            success: function( data ) {
                var books = [];
                for (i=0; i< data.length; i++) {
                    books.push({
                        value: data[i].id,
                        label: data[i].book_name + ', ' + data[i].author_name
                    })
                }
                response(books);
            }
        });
        },
        minLength: 1,
        select: function( event, ui ) {
          console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
        }
    });
});
