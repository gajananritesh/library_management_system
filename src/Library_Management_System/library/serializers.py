from rest_framework import serializers
from .models import Member,Books

class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model=Member
        # fields=('first_name', 'last_name')
        fields = '__all__'

class BooksSerializer(serializers.ModelSerializer):
    class Meta:
        model=Books
        # fields=('first_name', 'last_name')
        fields = '__all__'

