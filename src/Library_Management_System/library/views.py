from django.shortcuts import render, redirect, render_to_response, HttpResponse, get_object_or_404
from django.contrib.auth import authenticate, login,logout
from forms import LibrarianLoginForm, BookRegisterForm, MemberRegForm
from django.contrib.auth.models import User
from models import Books, Member, CopyOfBooks, BookTransaction
from django.template import RequestContext
from django.db.models import Q
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import MemberSerializer, BooksSerializer
import requests
from Library_Management_System import settings

class memberList(APIView):
    def get(self,request):
        member1 = Member.objects.all()
        serialiser=MemberSerializer(member1, many=True)
        return Response(serialiser.data)

class booksList(APIView):
    def get(self,request):
        book1 = Books.objects.all()
        serialiser=BooksSerializer(book1, many=True)
        return Response(serialiser.data)

class searchList(APIView):
    def get(self,request):
            search_query = request.GET.get('search')

            q_object = Q(book_name__icontains=search_query) | Q(author_name__icontains=search_query) | Q(id__contains=search_query)

            books = Books.objects.filter(q_object)
            # books = Books.objects.all()
            serialiser = BooksSerializer(books, many=True)

            return Response(serialiser.data)

def login_view(request):
    if request.user.is_authenticated():
        return redirect('/library')
    else:
        return render(request,'login.html')

def auth_view(request):
        user_name = request.POST['username']
        pass_word = request.POST['password']
        user = authenticate(request, username=user_name, password=pass_word)

        if user is not None:
            login(request, user)
            return redirect('manager_home')
        else:
            form = LibrarianLoginForm(request.POST)
            return render(request,'login.html',{'form':form, 'error_message': "You have entered wrong user id or password.",})

def manager_home(request):
    return render(request,'manager_home.html')   

def book_reg(request):
    return render(request,'book_reg.html')   

def insert_book(request):
    if request.method=='POST':
        form = BookRegisterForm(request.POST)  

        if form.is_valid():
            form.save()
            return render(request,'book_reg.html', {'form':form, 'success_message':"Book Data Inserted Successfully.",})
        
        else:
            return render(request,'book_reg.html', {'error_message':"Please insert valid data",})
    else:
        return render(request,'book_reg.html')

def register_view(request):
    return render(request,'register.html')

def view_book(request):
    books = Books.objects.all()
    args = { 'books':books}

    return render(request,'view_book_details.html', args)

def copy_count(request,book_id):
    
    b = Books.objects.get(id=book_id)
    c = b.copyofbooks_set.all()


    books = Books.objects.all()
    args = {'cs':c,'bs':b, 'books':books}
    return render(request,'view_book_details.html',args)
    # return HttpResponse("This is Working Fine for %s" % book_id)

def user_reg(request):
    if request.method =='POST':
        form = MemberRegForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return render(request, 'user_reg.html',{'success_message':'Member is Registered Successfully.'})
        else:
            return render(request, 'user_reg.html',{'error_message':'Enter Valid Data.'})

    else:
        form = MemberRegForm()
        return render(request, 'user_reg.html',{'form':form})

def issue_book(request):
    if request.method == 'POST':
        member_data = request.POST['mname']
        book_data = request.POST['bname']
        if (member_data == 'Member Name') or (book_data == 'Book Name'):
            data1 = Member.objects.all()
            data2 = Books.objects.all()

            args = {'data1s':data1, 'data2s':data2, 'error_message':'Please Select From Valid Options'}
            return render(request,'issue_book.html', args)
        else:
            m1 = Member.objects.get(id=member_data)
            b1 = Books.objects.get(id=book_data)

            c = b1.copyofbooks_set.filter(member_id__isnull=True).count()
            
            if c > 0:

                c1 = CopyOfBooks.objects.filter(books_id=book_data)
                c2 = c1.filter(member_id__isnull=True)
                
                m2 = m1.copyofbooks_set.all().count()
                
                if m2 < 1:
                    c3 = c2[0]
                    m1.copyofbooks_set.add(c3)      # added the member_id to copyofbooks.
                    BookTransaction.objects.create(status='Issued', copy=c3, member=m1)

                    data1 = Member.objects.all()
                    data2 = Books.objects.all()
            
                    args = {'data1s':data1, 'data2s':data2, 'success_message':'Book Issued Successfully.'}
                    return render(request,'issue_book.html', args)
                else:
                    return render(request,'issue_book.html', {'error_message':'Already taken max no of Books.'})
            else:
                data1 = Member.objects.all()
                data2 = Books.objects.all()

                args = {'data1s':data1, 'data2s':data2, 'error_message':'No Copies are avilable write now'}
                return render(request,'issue_book.html', args)

    else:
        data1 = Member.objects.all()
        data2 = Books.objects.all()

        args = {'data1s':data1, 'data2s':data2}
        return render(request,'issue_book.html', args)

def return_getmember(request):
    if request.method == 'POST':
        copy_data = request.POST['id']
        c1 = CopyOfBooks.objects.get(id=copy_data)
        c2 = c1.member_id

        m1 = Member.objects.get(id=c2)
        args = {'m1s':m1,'c1s':c1}
        return render(request, 'return_book.html', args)
    else:
        return render(request,'return_book.html')

def return_book(request):
    if request.method == 'POST':

        copy_data = request.POST['cid']
        member_data = request.POST['mid']
        
        c1 = CopyOfBooks.objects.get(id=copy_data)
        c1.member_id=None
        c1.save()
        m1 = Member.objects.get(id=member_data)

        BookTransaction.objects.create(status='Returned', copy=c1, member=m1)

        args = {'success_message':'Book is Returned Successfully.',}
        
        return render(request, 'return_book.html', args)

    else:
        args = {'error_message':'Book Returning Failed.',}
        return render(request,'return_book.html', args)

def search_user_taken(request):
    if request.method=='POST':
        member_data = request.POST['mname']
        book_data = request.POST['bname']
        if (member_data == 'Member Name') and (book_data == 'Book Name'):
            data1 = Member.objects.all()
            data2 = Books.objects.all()

            args = {'data1s':data1, 'data2s':data2, 'error_message':'Please Select From Valid Options'}
            return render(request,'search_user.html', args)
            
        if book_data == 'Book Name':
            m1 = Member.objects.get(id=member_data)
            
            m2 = m1.copyofbooks_set.all()
            
            args = {'datam2s':m2,'success_message':'Book Issued Status.'}
            return render(request,'search_user.html',args)
        
        else:
            b1 = Books.objects.get(id=book_data)

            b2 = b1.copyofbooks_set.all()
           
            args = {'datab2s':b2, 'success_message':'Book Issued Status.'}
            return render(request,'search_user.html',args)

    else:
        data1 = Member.objects.all()
        data2 = Books.objects.all()

        args = {'data1s':data1, 'data2s':data2}
        return render(request,'search_user.html',args)

def add_copy(request):
    if request.method == 'POST':
        book_data = request.POST['bname']
        if book_data != 'Select Book':

            b1 = Books.objects.get(id=book_data)
            c1 = CopyOfBooks.objects.create(books=b1)
            # b1.copyofbooks_set.add(c1)

            data2 = Books.objects.all()
            args = {'data2s':data2, 'success_message':'Added Successfully ...'}
            return render(request,'add_copy.html',args)
        else:
            data2 = Books.objects.all()
            args = {'data2s':data2, 'error_message':'Select a Valid Option ...'}
            return render(request,'add_copy.html',args)

    else:
        data2 = Books.objects.all()
        args = {'data2s':data2}
        return render(request,'add_copy.html',args)

def copy_book(request,copy_id):
    transactions = BookTransaction.objects.filter(copy_id=copy_id)
    
    paginator = Paginator(transactions, 5)
    page = request.GET.get('page')
    
    try:
        trans = paginator.page(page)
    except PageNotAnInteger as e:
        print(e)
        trans = paginator.page(1)
    except EmptyPage as e:
        print(e)
        trans = paginator.page(paginator.num_pages)

    args = {'transactions':transactions, 'trans':trans}
    return render(request, 'show_transaction.html', args)

def profile(request):
    if request.user.is_authenticated():
        # u = request.user.first_name
        profile = Member.objects.get(first_name='Gajanan')
        args = {'names':profile}
        return render(request,'profile.html', args)
    else:
        return HttpResponse("Please Login First ...")

def reg_user_view(request):
    if request.method == 'POST':
        form = LibrarianLoginForm(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            password=form.cleaned_data['password']      
            user.set_password(password)
            user.save()
            return render(request, 'success.html')
        else:
            form = LibrarianLoginForm(request.POST)
            return render(request,'register.html', {'form':form, 'error_message':"Please enter valid data",})

    else:
        # form = LibrarianLoginForm()
        return render(request,'register.html')

def logout_view(request):
    logout(request)
    return render(request,'logout.html')

def all_transactions(request):
    transactions = BookTransaction.objects.all()

    paginator = Paginator(transactions, 10)
    page = request.GET.get('page')
    
    try:
        trans = paginator.page(page)
    except PageNotAnInteger as e:
        print(e)
        trans = paginator.page(1)
    except EmptyPage as e:
        print(e)
        trans = paginator.page(paginator.num_pages)

    args = {'trans':trans}
    return render(request, 'all_transactions.html', args)

def map_view(request):
    return render(request, 'maps.html')

