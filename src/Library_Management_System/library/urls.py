
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^auth/$', views.auth_view, name='auth'),
    url(r'^manager_home/$', views.manager_home, name='manager_home'),
    url(r'^book_reg/$', views.book_reg, name='book_reg'),
    url(r'^insert_book/$', views.insert_book, name='insert_book'),
    url(r'^view_book/$', views.view_book, name='view_book'),

    url(r'^return_getmember/$', views.return_getmember, name='return_getmember'),
    url(r'^return_book/$', views.return_book, name='return_book'),
    url(r'^view_book/(?P<book_id>[0-9]+)/$', views.copy_count, name='copy_count'),
    url(r'^copy_book/(?P<copy_id>[0-9]+)/$', views.copy_book, name='copy_book'),

    url(r'^user_reg/$', views.user_reg, name='user_reg'),
    url(r'^issue_book/$', views.issue_book, name='issue_book'),

    url(r'^maps/$', views.map_view, name='maps'),

    url(r'^search_user_taken/$', views.search_user_taken, name='search_user_taken'),
    url(r'^add_copy/$', views.add_copy, name='add_copy'),

    url(r'^register/$', views.register_view, name='register'),
    url(r'^reg_user/$', views.reg_user_view, name='reg_user'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^all_transactions/$', views.all_transactions, name='all_transactions'),
    
    url(r'^admin/', admin.site.urls),
    url(r'^profile/$', views.profile, name='profile'),
]
