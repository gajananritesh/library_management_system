from django.db import models
from django.utils import timezone


class Books(models.Model):
    book_name = models.CharField(max_length=30,unique=True)
    author_name = models.CharField(max_length=30)
    year = models.IntegerField()
    date = models.DateField(timezone.now())
    price = models.IntegerField()

    class Meta:
        # verbose_name_plural = 'Books',
        ordering = ('book_name',)

    def __str__(self):
        return '{}, {}'.format(self.book_name, self.author_name)

class Member(models.Model):
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    mobile_no = models.IntegerField()
    address = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='media',height_field='height_field',width_field='width_field')
    height_field = models.IntegerField(default=0)
    width_field= models.IntegerField(default=0)

    def __str__(self):
        return self.first_name
    
    def full_name(self):
        return ' '.join([self.first_name, self.last_name])

class CopyOfBooks(models.Model):
    books = models.ForeignKey(Books, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE, null=True)
    def __str__(self):
        return self.books.book_name


class BookTransaction(models.Model):
    copy = models.ForeignKey(CopyOfBooks, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    status = models.CharField(max_length=10,default="Issued/Returned")
    date_time = models.DateTimeField(default=timezone.now())

    def __str__(self):
        return str(self.copy.id)

    class Meta:
        ordering = ('date_time',)

