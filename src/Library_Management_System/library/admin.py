# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models import Books,Member,CopyOfBooks

admin.site.register(Books)
admin.site.register(Member)
admin.site.register(CopyOfBooks)