# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-03 14:08
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0005_auto_20180903_1334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='books',
            name='book_id',
        ),
        migrations.AddField(
            model_name='books',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='books',
            name='date',
            field=models.DateField(verbose_name=datetime.datetime(2018, 9, 3, 14, 8, 0, 913201, tzinfo=utc)),
        ),
    ]
