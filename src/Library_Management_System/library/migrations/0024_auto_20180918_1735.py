# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-18 12:05
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0023_auto_20180918_1520'),
    ]

    operations = [
        migrations.DeleteModel(
            name='FixerExchangeRates',
        ),
        migrations.AlterField(
            model_name='books',
            name='date',
            field=models.DateField(verbose_name=datetime.datetime(2018, 9, 18, 12, 5, 54, 665747, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='booktransaction',
            name='date_time',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 18, 12, 5, 54, 672919, tzinfo=utc)),
        ),
    ]
