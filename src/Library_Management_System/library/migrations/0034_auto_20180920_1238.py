# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-20 07:08
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0033_auto_20180920_1236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='books',
            name='date',
            field=models.DateField(verbose_name=datetime.datetime(2018, 9, 20, 7, 8, 42, 55277, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='booktransaction',
            name='date_time',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 20, 7, 8, 42, 62419, tzinfo=utc)),
        ),
    ]
