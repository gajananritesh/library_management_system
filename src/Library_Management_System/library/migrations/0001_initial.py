# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-03 07:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Books',
            fields=[
                ('book_name', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('author_name', models.CharField(max_length=20)),
                ('year', models.IntegerField()),
                ('price', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Copy_of_Books',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('books', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Books')),
            ],
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=15)),
                ('last_name', models.CharField(max_length=15)),
                ('mobile_no', models.IntegerField()),
                ('address', models.CharField(max_length=50)),
                ('photo', models.ImageField(upload_to=b'')),
            ],
        ),
    ]
